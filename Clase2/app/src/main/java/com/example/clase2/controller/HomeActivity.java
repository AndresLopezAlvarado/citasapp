package com.example.clase2.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.clase2.Globales.Global;
import com.example.clase2.R;

public class HomeActivity extends AppCompatActivity {
    Global global = new Global();
    TextView tvNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        tvNombre.setText(global.nombre);
    }
}