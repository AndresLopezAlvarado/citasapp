package com.example.clase2.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.example.clase2.R;
import com.example.clase2.adapters.AdapterUser;
import com.example.clase2.models.mUser;

import java.util.ArrayList;
import java.util.List;

public class UserActivity extends AppCompatActivity {
    RecyclerView rvListUser;
    List<mUser> mDataUsers;
    AdapterUser adapterUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        OBJETOS();
        DATA();
    }

    private void OBJETOS(){
        rvListUser = findViewById(R.id.rvListUser);
    }

    private void DATA() {
        mDataUsers = new ArrayList<>();

        mDataUsers.add(new mUser("1", "Carolina Gomez", "Hola a todos", "Mujer",
                "https://goo.gl/gEgYUd", "01/01/2000", "Medellín",
                "3223456789", "Amistad",  "23"));
        mDataUsers.add(new mUser("1", "Fernanda Avendaño", "Hola a todos", "Mujer",
                "https://goo.gl/gEgYUd", "01/01/2000", "Medellín",
                "3223456789", "Amistad",  "23"));
        mDataUsers.add(new mUser("1", "Luisa Montoya", "Hola a todos", "Mujer",
                "https://goo.gl/gEgYUd", "01/01/2000", "Medellín",
                "3223456789", "Amistad",  "23"));
        mDataUsers.add(new mUser("1", "Jhon Alexander", "Hola a todos", "Hombre",
                "https://goo.gl/gEgYUd", "01/01/2000", "Medellín",
                "3223456789", "Amistad",  "23"));

        adapterUsers = new AdapterUser(this, mDataUsers);
        rvListUser.setAdapter(adapterUsers);
        rvListUser.setLayoutManager(new LinearLayoutManager(this));
    }
}