package com.example.clase2.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clase2.Globales.Global;
import com.example.clase2.R;

public class LoginActivity extends AppCompatActivity {
    TextView etEmail;
    Button btnLogin, btnRegistro;
    Global global = new Global();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        etEmail = findViewById(R.id.etEmail);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegistro = findViewById(R.id.btnRegistro);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etEmail.getText().toString().isEmpty()) {
                    Toast toast = Toast.makeText(LoginActivity.this, "El campo email esta vacio",
                            Toast.LENGTH_SHORT);
                    toast.show();
                } else if(etEmail.getText().toString().equals(global.email)) {
                    Toast toast = Toast.makeText(LoginActivity.this, "Bienvenido",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    Intent intent = new Intent(LoginActivity.this, UserActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });
    }
}