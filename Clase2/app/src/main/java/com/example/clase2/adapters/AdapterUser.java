package com.example.clase2.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.example.clase2.R;
import com.example.clase2.models.mUser;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.MyViewHolder> {
    private Context mContext;
    List<mUser> mData;

    public AdapterUser(Context mContext, List<mUser> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.userprofileview, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.tvEdad.setText(mData.get(position).getEdad());
        holder.tvNombre.setText(mData.get(position).getNombre());
        holder.tvGenero.setText(mData.get(position).getGenero());
        Glide.with(mContext).load(mData.get(position).getImagen()).into(holder.image);

        holder.userProfileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence text = mData.get(position).getNombre();
                mensaje(text.toString());
            }
        });

        holder.btnUnlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { mensaje("Dislike"); }
        });

        holder.btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mensaje("Like");
            }
        });

        holder.mLottie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mLottie.setSpeed(2);
                holder.mLottie.loop(false);
                holder.mLottie.playAnimation();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void mensaje(String texto){
        CharSequence text = texto;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(mContext, text, duration);
        toast.show();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNombre, tvEdad, tvGenero;
        ImageView image;
        CardView userProfileView;
        FloatingActionButton btnUnlike, btnLike;
        LottieAnimationView mLottie;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvEdad = itemView.findViewById(R.id.tvEdad);
            tvNombre = itemView.findViewById(R.id.tvNombre);
            tvGenero = itemView.findViewById(R.id.tvGenero);
            btnLike = itemView.findViewById(R.id.btnLike);
            btnUnlike = itemView.findViewById(R.id.btnUnlike);
            mLottie = itemView.findViewById(R.id.favoriteAnimation);
            userProfileView =  itemView.findViewById(R.id.userProfileView);
            image = itemView.findViewById(R.id.imgUser);
        }
    }
}